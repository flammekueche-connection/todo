AS1: J'utilise indistinctement féminin et masculin pour parler de il/iel/elle.\
AS2: c'est mal écrit et mal présenté (RERO)


Ce texte tente de répertorier l'intégralité des projets/demandes
que je retiens dans une todo forcément déjà trop longue:
il me tient à coeur de répondre et/ou au moins par de la mise en relation.

Attention: cette liste n'est pas un engagement de ma part. Si tu tiens à
ce qu'un projet aboutisse dans des délais raisonables: évangélise, mobilise,
actionne, participe. Si tu te crois illégitime, tu as tord!  (cf: CP porter un
projet).

Tu as une nouvelle idée? (soft, event, ...)?

* si tu es assez forte en gueule: viens la défendre sur la liste lug@ ou pendant
  une réunion de FC, hackstub, ... pour trouver des porteurs. Ta proposition doit
  au moins contenir deux volets:
  * à quoi ca sert ?
  * des débuts de solutions ou des personnes qui sont probablement ressources.
  idéalement, il faut ajouter:
  * faire un tour de l'existant
  Si possible rendre compte des conclusions de discussions sur lug@
* si t'es timide ou que tu as pris un gros vent: tu peux toujours venir m'en parler.

T'es intéressée par qqchose ? contacte-moi.

# Dans le tube

## en approche

* conférence JDS
* com. JNA
* table ronde "2eme interasso d'alsace"
* réponse "parler de numérique au lycée"
* CP "Antisocial?", CP la pollution numérique: outils et stragégies

## autres

* réponse à la demande de reformation de sxb.so (feedback+table ronde+proposition)
* version associative de Places (avec sens, cesar,) (et contact web agencies?)
* relancer les status de lilo ? (qui en a vraiment le courage?)

## Evenements à venir

CT: conférence technique
CP: conférence grand public
TR: table ronde (forcément publique et en ligne facon bitreich?) + CR

CP comment contribuer? (matu?) (RERO, scratch, JRES2017, contributopia)\
TR état de la réflexion de/sur PIC (et liens avec SL/BR)\
TR stuck market (jybe? multikulti?)\
TR RMLLML (RMLL Mais Little) (harmonie?)\
TR web is dead, all of them\
TR Katzele: le petit chaton sans prétention\
CP (porte-voix?) porter un projet: tous légitimes, tous humains!\
CT acme changed my life: la VF\
CT ma relation avec C/C++ (c'est compliqué) ...\
CT mon "nouveau" langage en 20mn? (go, zig, crystal, raku, purescript, haskell, ...)\
CT Quo Vadis, Perl (odin)\
CT Accessibili/tty (odin, irina?)

# cookbook organiser les évenements alsaciens

A rédiger mais en gros
* trouver le référent FC (TODO:contacts)
* trouver un lieu (TODO:contacts)
* produire la communication
* activer les relais (TODO:contacts) (facebook, demosphere, agenda du libre)
* aider les chatons a se filer des events, supports? contact frama ?

Cartographie (simple dot?) de nos relais potentiels et effectifs
(trouver les relais et partenaires educ pop :recontacter FTLieux, Frama, petits D ...)

# Katzele

* avoir une infra et commencer à la tester entre nous au plus vite
* règles et autoévaluation (producteurs=consommateurs)

# PIC

(pousseurs/promoteurs d'inclusion convivialiste) (pic fait référence à l'outils
mais aussi aux guides de haute montagne)

* sensei (avec bboet,sens,cesar)
* galeroff (avec karchnu,odin,ecoinfo)
* pic-office (agenda partagés, ...)

CP parler de illich ? (matu)

# Règles/idées de communication

je tente de trouver des accronymes pour les problèmes dont on parle en boucle
pour diverses raisons:

## RERO: mieux que rien (release early, release often?)

* il vaut mieux que ca dorme publiquement (et succite réflexion, débat ou
  contribution) que sur mon disque dur.
* si tu peux aider/faire mieux: lache toi!
  * nous sommes tous légitimes
  * meme si ta contrib est refusée, elle exprime une opinion que l'auteur intègrera dans son
    schema de pensée.
* si tu as une critique, communique sous la forme d'une attente, d'une alternative disponible
* si tu veux t'occuper d'un dossier:
  * interroge ta capacité à resté mobilisé dessus
  * idéalement: trouve un binôme

## MISS

Make it short and straight (équivalent KISS pour la com?)

* les formulations les plus courtes sont toujours les meilleures
* si tu es trop long, c'est peut-être que ton propos devrait etre explosé en plusieurs
  thématiques
* dans une introduction: ne paraphrase pas, ne réexplique pas: site
* avant de parler du comment, sois sur que le pourquoi soit clair pour tout le monde
  ou détaillé quelque part.
