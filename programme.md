# En construction

# Tables rondes

## Rencontre autour d'Ivan Illich

Unix est connu pour être la fondation technologique d'Internet
et le système d'exploitation le plus utilisé au monde. Si son
immense valeur économique et technique ne font plus débat,
les technophiles alertent sur la dissolution de la principale
valeur d'unix dans la culture numérique actuelle: sa philosophie
et la vision que ses créateurs portaient sur les interactions
homme-machine.

En découvrant Ivan Illich et son convivialisme, ces technophiles
se reconnaisent dans la critique des modifications récentes du
rapport que l'homme entretient à l'outils.

L'invocation d'Illich pour parler d'un "convivialisme numérique"
par les technophiles est-elle justifiée et quelles sont vraiment
les points communs entre la culture qui se transmet dans le monde
Unix et la pensée d'Illich? Cette table ronde se veut être une
rencontre entre les technophiles et les sciences humaines pour
redécouvrir Illich au travers de ses questions.


